<?php 
    namespace App\Http\Controllers\NfsController;
    use App\Nf;
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;

    class Nfs extends Controller{

        
       public function index(Request $request) {
           
            $nfs = Nf::query()
            ->orderBy('nf_razao_social')
            ->get();

            $msg = $request->session()->get('msg');

           return view('nfs.index', compact('nfs','msg'));
        }

        public function create(){
            return view('nfs.create');
        }

        public function store(Request $request){
            $nf = new Nf();
            
            $numeroNf = $request->numeroNf;
            $nf->nf_numero = $request->numeroNf;
            
            $nf_razao_social = $request->razaoSocial;
            $nf->nf_razao_social = $request->razaoSocial;
            
            $nf_cnpj = $request->cnpj;
            $nf->nf_cnpj = $request->cnpj;
            
            $nf_valor = $request->valor;
            $nf->nf_valor = $request->valor;

            $dt_vencimento = $request->dateVenc;
            $nf->dt_vencimento = $request->dateVenc;
            
            $observao = $request->obs;
            $nf->observao = $request->obs;


            $nf::create([
                'nf_numero' => $numeroNf,
                'nf_razao_social'=> $nf_razao_social,
                'nf_cnpj' => $nf_cnpj,
                'nf_valor' => $nf_valor,
                'dt_vencimento' => $dt_vencimento,
                'observao' => $observao
            ]);
            
            $request->session()
            ->flash(
                'msg',
                "NF com número {$nf->nf_numero} inserida com sucesso!"
            );

            return redirect()->route('listar_nfs');
                    
        }

        public function destroy(Request $request){
            
            Nf::destroy($request->id);

            $request->session()
            ->flash(
                'msg',
                "NF removida com sucesso!"
            );

            return redirect()->route('listar_nfs');
        }

        public function viewEdit(Request $request){
            $row = NF::query('nf')->where('id', $request->id)->get();
            return view('nfs.edit', compact('row'));

        }

        public function edit(Request $request) {
            $nf = new Nf();
            
            $numeroNf = $request->numeroNf;
            $nf->nf_numero = $request->numeroNf;
            
            $nf_razao_social = $request->razaoSocial;
            $nf->nf_razao_social = $request->razaoSocial;
            
            $nf_cnpj = $request->cnpj;
            $nf->nf_cnpj = $request->cnpj;
            
            $nf_valor = $request->valor;
            $nf->nf_valor = $request->valor;

            $dt_vencimento = $request->dateVenc;
            $nf->dt_vencimento = $request->dateVenc;
            
            $observao = $request->obs;
            $nf->observao = $request->obs;
            
            $row = NF::query('nf')->where('id', $request->id)
            ->update([
                'nf_numero' => $numeroNf,
                'nf_razao_social'=> $nf_razao_social,
                'nf_cnpj' => $nf_cnpj,
                'nf_valor' => $nf_valor,
                'dt_vencimento' => $dt_vencimento,
                'observao' => $observao
            ]);


            $request->session()
            ->flash(
                'msg',
                "NF atualizada com sucesso!"
            );

            return redirect()->route('listar_nfs');
            
        } 

    }