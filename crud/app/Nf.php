<?php 
namespace App;

use Illuminate\database\Eloquent\Model;

    class Nf extends Model {
        protected $table = "nf";
        protected $fillable = [
            'nf_numero',
            'nf_razao_social',
            'nf_cnpj',
            'nf_valor',
            'dt_vencimento',
            'observao'
        ];
    }

?>