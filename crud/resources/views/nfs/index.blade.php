@extends('layout')

@section('header')
Nf-s
@endsection

@section('content')

@if(!empty($msg))
<div class="alert alert-success">
    {{$msg}}
</div>
@endif
@if(!empty($msgRemove))
<div class="alert alert-danger">
    {{$msgRemove}}
</div>
@endif

<div class="d-flex flex-row-reverse">
    <p class="lead">
        <a class="btn btn-primary btn-md" href="{{route('form_add_nf')}}" role="button">Nova Nota</a>
    </p>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Número</th>
            <th>Razão Social </th>
            <th>CNPJ</th>
            <th>Valor</th>
            <th>Data de Vencimento</th>
            <th>Observação</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach($nfs as $nf)
        <tr>
            <td class="">{{$nf->nf_numero}}</td>
            <td class="">{{$nf->nf_razao_social}}</td>
            <td class="">{{$nf->nf_cnpj}}</td>
            <td class="">{{$nf->nf_valor}}</td>
            <td class="">{{$nf->dt_vencimento}}</td>
            <td class="">{{$nf->observao}}</td>
            <td class="">
                <div class="row">
                    <div class="col" style="margin:1px;">
                        <form action=" nfs/remove/{{$nf->id}}" method="post"
                            onsubmit="return confirm('Tem certeza que deseja apagar {{addslashes($nf->nf_numero)}}')">
                            @csrf
                            @method('DELETE')
                            <button class=" btn btn-danger"><i class="fas fa fa-trash-o	"
                                    style="font-size:15px; "></i></button>


                        </form>
                    </div>
                    <div class="col">
                        <a class=" btn btn-danger" href="/nfs/edit/{{$nf->id}}"><i class="fa fa-edit"
                                style="font-size:15px;"></i></a>
                    </div>
                </div>


            </td>
        </tr>
        @endforeach
    </tbody>

</table>
</div>

@endsection