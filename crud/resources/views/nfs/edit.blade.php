@extends('layout')
@section('header')
Editar NF-s
@endsection


@section('content')
<!-- {{$row}} -->
<form method="post">
    @csrf
    @foreach($row as $value)

    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Número da NF</label>
        <input type="text" class="form-control" name="numeroNf" value="{{$value->nf_numero}}">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Razão Social</label>
        <input type="text" class="form-control" name="razaoSocial" value="{{$value->nf_razao_social}}">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">CNPJ</label>
        <input type="text" class="form-control" name="cnpj" value="{{$value->nf_cnpj}}">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Valor</label>
        <input type="text" class="form-control" name="valor" value="{{$value->nf_valor}}">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Data Vencimento </label>
        <input type="date" class="form-control" name="dateVenc" value="{{$value->dt_vencimento}}">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Observação</label>
        <input type="text" class="form-control" name="obs" value="{{$value->observao}}">
    </div>
    <div class="d-flex flex-row-reverse">
        <button class="btn btn-primary btn-md m-2">Salvar</a>
    </div>
    @endforeach

</form>
@endsection