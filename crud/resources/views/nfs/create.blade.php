@extends('layout')
@section('header')
Cadastrar NF-s
@endsection

@section('content')
<form method="post">
    @csrf
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Número da NF</label>
        <input type="text" class="form-control" name="numeroNf">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Razão Social</label>
        <input type="text" class="form-control" name="razaoSocial">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">CNPJ</label>
        <input type="text" class="form-control" name="cnpj">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Valor</label>
        <input type="text" class="form-control" name="valor">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Data Vencimento </label>
        <input type="date" class="form-control" name="dateVenc">
    </div>
    <div class="form-group ">
        <label for="" class="mt-2 mb-2">Observação</label>
        <input type="text" class="form-control" name="obs">
    </div>
    <div class="d-flex flex-row-reverse">
        <button class="btn btn-primary btn-md m-2">Salvar</a>
    </div>
</form>
@endsection