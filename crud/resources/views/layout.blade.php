<!doctype html>
<html lang="pt-BR">

<head>
    <meta charset="UTF=8">
    <meta name="viewport" content="width=device-with, user-scalable=no, initial-scale=1.0">
    </meta>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    </meta>
    <title>Crud</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="/your-path-to-fontawesome/css/fontawesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
.row .col {
    /* margin-left: -1px; */
    /* background-color: blue; */
    width: 10px;
    margin: 1px;
}
</style>

<body>
    <div class="container">
        <div class="jumbotron mt-5">
            <h1>@yield("header")</h1>
            <hr class="my-2">
        </div>

        @yield("content")


    </div>
</body>

</html>