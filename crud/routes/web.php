<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/nfs', 'NfsController\Nfs@index')->name('listar_nfs');
Route::get('/nfs/create', 'NfsController\Nfs@create')->name('form_add_nf');
Route::post('/nfs/create', 'NfsController\Nfs@store');
Route::delete('/nfs/remove/{id}', 'NfsController\Nfs@destroy');
Route::get('/nfs/edit/{id}', 'NfsController\Nfs@viewEdit');
Route::post('/nfs/edit/{id}', 'NfsController\Nfs@edit');