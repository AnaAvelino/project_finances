<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Nf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nf',function (Blueprint $table){
            $table->increments('id');
            $table->string('nf_numero');
            $table->string('nf_razao_social');
            $table->string('nf_cnpj');
            $table->double('nf_valor');
            $table->date('dt_vencimento');
            $table->string('observao');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nf');
    }
}